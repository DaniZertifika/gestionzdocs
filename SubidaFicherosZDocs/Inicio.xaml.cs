﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;

namespace SubidaFicherosZDocs
{
    /// <summary>
    /// Interaction logic for Inicio.xaml
    /// </summary>
    public partial class Inicio : Window
    {
        public Inicio()
        {
            InitializeComponent();
        }

        private void BTSupervisor_Click(object sender, RoutedEventArgs e)
        {

            Supervisor sup = new Supervisor();
            sup.Show();
            this.Close();

        }

        private void BTUsuario_Click(object sender, RoutedEventArgs e)
        {

            Usuario Usu = new Usuario();
            Usu.Show();
            this.Close();

        }
    }
}

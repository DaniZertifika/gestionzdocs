﻿using Microsoft.Win32;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;
using System.Windows.Threading;

namespace SubidaFicherosZDocs
{
    /// <summary>
    /// Interaction logic for Usuario.xaml
    /// </summary>
    public partial class Usuario : Window
    {
        WSPro.Service1 ws = new WSPro.Service1();
        private static string ruta = "";

        public Usuario()
        {
            InitializeComponent();
        }







        private void BTNAtras_Click(object sender, RoutedEventArgs e)
        {
            Inicio ini = new Inicio();
            ini.Show();
            this.Close();
        }

        private void BTNAlta_Click(object sender, RoutedEventArgs e)
        {
            string Nombre = TBNombre.Text;
            if (Nombre.Equals(""))
            {
                campoErroneo("Nombre");
                return;
            }

            string Apellidos = TBApellidos.Text;
            if (Apellidos.Equals(""))
            {
                campoErroneo("Apellidos");
                return;
            }

            string Email = TBEmail.Text;
            if (Email.Equals(""))
            {
                campoErroneo("Email");
                return;
            }

            string PuestoTrabajo = TBPuestoTrabajo.Text;
            if (PuestoTrabajo.Equals(""))
            {
                campoErroneo("Puesto de Trabajo");
                return;
            }

            string Password = "Zertifika";
            string Dni = TBDNI.Text;
            if (Dni.Equals(""))
            {
                campoErroneo("DNI");
                return;
            }

            string IdDistribuidor = "1999";
            string TokenTemporal = ""; //ni idea de que es
            string FechaAlta = DateTime.Now.ToString();
            string PasswordEncriptada = ""; // se enccripta si se crea correctamente el usuario
            string Cif = TBDNI.Text;

            string Calle = TBCalle.Text;
            if (Calle.Equals(""))
            {
                campoErroneo("Calle");
                return;
            }

            string Poblacion = TBPoblacion.Text;
            if (Poblacion.Equals(""))
            {
                campoErroneo("Población");
                return;
            }

            string Provincia = "Islas Baleares";
            string Pais = "España";
            string Cp = TBCP.Text;
            if (Cp.Equals("")) { campoErroneo("CP"); return; }

            string Telefono = TBTelefono.Text;
            if (Telefono.Equals("")) { campoErroneo("Teléfono"); return; }

            string Web = "www.signes30.com/ZdocsPro";
            if (Funciones.ExisteUsuarioZDocs(Email) == "" && Funciones.ExisteEmpresaZDocs(Dni) == "")
            {
                string idUsuario = Funciones.altaUsuario(Nombre, Apellidos, Email, PuestoTrabajo, Password, Dni, IdDistribuidor, TokenTemporal, FechaAlta, PasswordEncriptada);
                if (!idUsuario.Equals("-1"))
                {
                    string idEmpresa = Funciones.altaEmpresa(Nombre, Cif, Calle, Poblacion, Provincia, Pais, Cp, Email, Telefono, Web);
                    if (!idEmpresa.Equals("-1"))
                    {
                        string idUsuarioEmpresa = Funciones.crearUsuarioEmpresa(idUsuario, idEmpresa);
                        MessageBox.Show("Usuario creado Correctamente!");
                    }
                    else
                    {
                        MessageBox.Show("Error al crear la empresa del Usuario " + Nombre);
                    }
                }
                else
                {
                    MessageBox.Show("Error al crear el usuario " + Nombre);
                }

            }
            else
            {
                MessageBox.Show("Error al crear el usuario, puede que ya exista este Email o DNI.");
            }

        }


        private void campoErroneo(string nombreCampo)
        {
            MessageBox.Show("El campo " + nombreCampo + " esta vacio.");
        }

        private void BTNConsulta_Click(object sender, RoutedEventArgs e)
        {
            try
            {
                //Dispatcher.Invoke(DispatcherPriority.Background, new Action(() => ));

                string[] resultado = null;
                if (!TBNombreConsulta.Text.Equals(""))
                {
                    resultado = ws.getUsuersByName(TBNombreConsulta.Text);
                }
                else if (!TBEmailConsulta.Text.Equals(""))
                {
                    resultado = ws.getUsuersByEmail(TBEmailConsulta.Text);
                }
                else
                {
                    MessageBox.Show("Los campos estan vacios.");
                    return;
                }

                listBox.Items.Clear();
                foreach (var item in resultado)
                {
                    listBox.Items.Add(item);
                }

            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
            finally
            {
                cargaCU.Content = "";
            }


        }

        private void tabControl_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {

        }

        private void BTNExplorar_Click(object sender, RoutedEventArgs e)
        {
            ruta = "";
            Microsoft.Win32.OpenFileDialog dlg = new Microsoft.Win32.OpenFileDialog();
            dlg.DefaultExt = ".pdf";
            dlg.Multiselect = true;
            Nullable<bool> result = dlg.ShowDialog();
            if (result == true)
            {

                string[] filenames = dlg.FileNames;
                foreach (string filename in filenames)
                {
                    ruta += filename + "|";
                }
            }
            TBRuta.Text = ruta;
        }

        private void BTNSubirDocumento_Click(object sender, RoutedEventArgs e)
        {
            int i = 0;
            if (!ruta.Equals(""))
            {
                if (!TBEmailSubida.Text.Equals(""))
                {
                    string idUsuario = Funciones.getIdUsuario(TBEmailSubida.Text);
                    if (!idUsuario.Equals("-1"))
                    {
                        string idEmpresa = Funciones.getIdEmpresa(idUsuario);
                        if (!idEmpresa.Equals("-1"))
                        {
                            string[] files = ruta.Split('|');
                            foreach (string fichero in files)
                            {
                                if (!string.IsNullOrEmpty(fichero))
                                {
                                    byte[] bytes = File.ReadAllBytes(fichero);
                                    string filename = fichero.Split('\\')[fichero.Split('\\').Length - 1].Replace("|", "");

                                    string resultado = Funciones.subirFichero(bytes, filename, idUsuario, "", idEmpresa, ".pdf", "");
                                    i++;
                                    if (!string.IsNullOrEmpty(resultado)) //ha ido bien
                                    {
                                        MessageBox.Show("Fichero: " + filename + " subido correctamente");
                                    }
                                    if (i >= 20)
                                    {
                                        break;
                                    }
                                }

                            }
                        }
                        else
                        {
                            MessageBox.Show("El usuario no tiene empresa. Consultar con Soporte");
                        }
                    }
                    else
                    {
                        MessageBox.Show("Email no encontrado.");
                    }
                }
                else
                {
                    MessageBox.Show("Email vacio.");
                }
            }
            else
            {
                MessageBox.Show("Ruta vacia.");
            }



        }

        private void BTNConsultarDocumentos_Click(object sender, RoutedEventArgs e)
        {

            string[] resultado = null;
            if (!TBEmailConsultaDocumentos.Text.Equals(""))
            {
                listBox1.Items.Add("Buscando documentos ...");
                resultado = ws.getDocumentsByEmail(TBEmailConsultaDocumentos.Text);
            }
            else
            {
                MessageBox.Show("El campo esta vacio.");
                return;
            }

            listBox1.Items.Clear();

            if (resultado.Length == 0)
            {
                MessageBox.Show("No tiene Documentos");
            }

            foreach (var item in resultado)
            {
                listBox1.Items.Add(item);
            }




        }

        private void BTNSubDoc_Click(object sender, RoutedEventArgs e)
        {
            string email;
            if (listBox.SelectedIndex >= 0)
            {
                email = listBox.SelectedValue.ToString().Split('-')[3].Trim();
                TBEmailSubida.Text = email;
                Dispatcher.BeginInvoke((Action)(() => tabControl.SelectedIndex = 2));
            }
        }

        private void BTNconsDOCUSU_Click(object sender, RoutedEventArgs e)
        {
            string email;
            if (listBox.SelectedIndex >= 0)
            {
                email = listBox.SelectedValue.ToString().Split('-')[3].Trim();
                TBEmailConsultaDocumentos.Text = email;
                Dispatcher.BeginInvoke((Action)(() => tabControl.SelectedIndex = 3));
                BTNConsultarDocumentos_Click(sender, e);
            }
        }
    }
}

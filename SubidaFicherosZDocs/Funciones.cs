﻿using SubidaFicherosZDocs.Clases;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Odbc;
using System.IO;
using System.Linq;
using System.Net.Mail;
using System.Text;
using System.Threading.Tasks;

namespace SubidaFicherosZDocs
{
    class Funciones
    {

        public const string CadenaConexionAnywhere = "Provider=SQLNCLI11.1;Server=tcp:192.168.1.95;Database=SignesPro;Uid=edoc;Pwd=edoc;Encrypt=True;Connection Timeout=3000;TrustServerCertificate=True";
        public const string CadenaConexionAnywhereSinProvider = "Server=tcp:192.168.1.95;Database=SignesPro;Uid=edoc;Pwd=edoc;Encrypt=True;Connection Timeout=3000;TrustServerCertificate=True";
        static WSPro.Service1 WS = new WSPro.Service1();
        static WorkFlow.WorkFlow WF = new WorkFlow.WorkFlow();


        public static string CargarExcelMarratxi(string Ruta)
        {
            Ruta += "\\ALTA ATM.xlsx";
            string CadenaConexionExcel = @"driver={Microsoft Excel Driver (*.xls, *.xlsx, *.xlsm, *.xlsb)};
            driverid=1046;dbq=" + Ruta;    //C:\Dev\EXCEL\Plantilla alta fotos.xlsx"; 

            DataSet ds = null;
            int Num_Empresas = 0;
            string IdUsuario, IdEmpresa = "";
            string Incidencias = "";
            int numIncidencias = 0;
            string arrayIncidencias = "";
            string Password = "";
            int i = 0;

            string allowedChars = "abcdefghijkmnopqrstuvwxyzABCDEFGHJKLMNOPQRSTUVWXYZ0123456789";
            char[] chars = new char[8];
            Random rd = new Random();



            try
            {
                using (OdbcConnection conn = new OdbcConnection(CadenaConexionExcel))
                {
                    conn.Open();
                    using (OdbcCommand command = conn.CreateCommand())
                    {
                        command.CommandText = "SELECT * FROM [Hoja1$]";
                        OdbcDataAdapter ad = new OdbcDataAdapter(command);
                        ds = new DataSet();
                        ad.Fill(ds);
                        var DNITitular = "";
                        foreach (DataRow dr in ds.Tables[0].Rows)
                        {
                            try
                            {


                                //var IDContrato = dr["IDContrato"].ToString();
                                var NombreTitular = "";
                                var Apellidos = "";
                                try
                                {
                                    if (dr["NombreTitular"].ToString().Contains("S.L.") || dr["NombreTitular"].ToString().Contains("S.A.")) //si es una empresa, que tiene un S.L., se queda con el nombre
                                    {
                                        NombreTitular = dr["NombreTitular"].ToString();
                                    }
                                    else //sino es una persona y se separa por nombre y apellido
                                    {
                                        NombreTitular = dr["NombreTitular"].ToString().Split(',')[1];
                                        Apellidos = dr["NombreTitular"].ToString().Split(',')[0];
                                    }

                                }
                                catch (Exception ex) //si falla, se queda sin apellidos, porque viene sin comas
                                {
                                    NombreTitular = dr["NombreTitular"].ToString();
                                }


                                DNITitular = dr[2].ToString();
                                var TelefonoTitular1 = "";
                                //var TelefonoTitular2 = dr["TelefonoTitular2"].ToString();
                                //var FaxTitular = dr["FaxTitular"].ToString();
                                //var MovilTitular = dr["MovilTitular"].ToString();
                                var EmailTitular = dr["EmailTitular"].ToString();
                                var Calle = dr["Direccion"].ToString();
                                //var Numero = dr["Numero"].ToString();
                                //var Escalera = dr["Escalera"].ToString();
                                //var Piso = dr["Piso"].ToString();
                                //var Puerta = dr["Puerta"].ToString();
                                //var CP = dr["CP"].ToString();
                                //var Poblacion = dr["Poblacion"].ToString();
                                //var fechaBaja = dr["fechaBaja"].ToString();

                                for (int j = 0; j < 8; j++)
                                {
                                    chars[j] = allowedChars[rd.Next(0, allowedChars.Length)];
                                }

                                Password = string.Join("", chars);

                                NombreTitular = NombreTitular.Replace("'", "");
                                Calle = Calle.Replace("'", "");
                                //Poblacion = Poblacion.Replace("'", "");

                                if (EmailTitular != "" && DNITitular != "")
                                {
                                    if (WS.ExisteUsuarioZDocs(EmailTitular) == "" && WS.ExisteEmpresaZDocs(DNITitular) == "")
                                    {
                                        //InsertarLinea(Consulta)ddd;
                                        try
                                        {
                                            //IdEmpresa = ws.CrearEmpresaZDocs("Aguas de Marratxi", "A07062409", "C/Juan XXIII, 2", "Marratxi", "Illes Balears", "ES", "07141", "administracion@aguasmarratxi.com", "971 723 620", "http://aguasmarratxi.com/");
                                            IdEmpresa = WS.CrearEmpresaZDocs(NombreTitular, DNITitular, Calle, "", "Illes Balears", "ES", "", EmailTitular, TelefonoTitular1, "");
                                            IdUsuario = WS.CrearUsuarioZDocs(NombreTitular, Apellidos, EmailTitular, "", Password, DNITitular, "1024", "", DateTime.UtcNow.Date.ToString(), Password);
                                            WS.CrearUsuarioEmpresaZDocs(IdUsuario, IdEmpresa, false, true); //administrador, principal



                                            EnviarCorreo(EmailTitular, "Alta en la Plataforma ZDocs", CrearCuerpoCorreoAlta("Aguas de Marratxi", Password), "");
                                            if (IdEmpresa == "-1" || IdUsuario == "-1")
                                            {
                                                arrayIncidencias += DNITitular + "|";
                                                numIncidencias++;
                                            }
                                            else
                                            {
                                                Num_Empresas += 1;
                                            }



                                        }
                                        catch (Exception ex)
                                        {
                                            arrayIncidencias += DNITitular + "|";
                                            numIncidencias++;
                                        }

                                    }
                                    else //ya existe el usuario o empresa
                                    {
                                        //arrayIncidencias += DNITitular + "|";
                                        numIncidencias++;
                                    }
                                }
                                i++;
                            }
                            catch (Exception ex)
                            {
                                arrayIncidencias += DNITitular + "|";
                                numIncidencias++;

                            }
                        }

                        Incidencias = "Numero de lineas Procesadas= " + Num_Empresas + ", Incidencias: " + numIncidencias;
                        return Incidencias;
                    }
                }

            }
            catch (Exception ex)
            {
                return ex.Message;
            }

        }






        public static string actualizarUsuarios(string Ruta)
        {
            Ruta = Ruta + "\\ALTA ATM.xlsx";
            string CadenaConexionExcel = @"driver={Microsoft Excel Driver (*.xls, *.xlsx, *.xlsm, *.xlsb)}; driverid=1046;dbq=" + Ruta;    //C:\Dev\EXCEL\Plantilla alta fotos.xlsx"; 

            DataSet ds = null;
            int Num_Empresas = 0;
            string IdUsuario, IdEmpresa = "";
            string Incidencias = "";
            int numIncidencias = 0;
            string arrayIncidencias = "";
            string Password = "";
            int i = 0;
            string allowedChars = "abcdefghijkmnopqrstuvwxyzABCDEFGHJKLMNOPQRSTUVWXYZ0123456789";
            char[] chars = new char[8];
            Random rd = new Random();

            try
            {
                using (OdbcConnection conn = new OdbcConnection(CadenaConexionExcel))
                {
                    conn.Open();
                    using (OdbcCommand command = conn.CreateCommand())
                    {
                        command.CommandText = "SELECT * FROM [Hoja1$]";
                        OdbcDataAdapter ad = new OdbcDataAdapter(command);
                        ds = new DataSet();
                        ad.Fill(ds);
                        var DNITitular = "";
                        foreach (DataRow dr in ds.Tables[0].Rows)
                        {
                            try
                            {
                                //var IDContrato = dr["IDContrato"].ToString();
                                var NombreTitular = "";
                                var Apellidos = "";
                                try
                                {
                                    if (dr["NombreTitular"].ToString().Contains("S.L.") || dr["NombreTitular"].ToString().Contains("S.A.")) //si es una empresa, que tiene un S.L., se queda con el nombre
                                    {
                                        NombreTitular = dr["NombreTitular"].ToString();
                                    }
                                    else //sino es una persona y se separa por nombre y apellido
                                    {
                                        NombreTitular = dr["NombreTitular"].ToString().Split(',')[1];
                                        Apellidos = dr["NombreTitular"].ToString().Split(',')[0];
                                    }
                                }
                                catch (Exception ex) //si falla, se queda sin apellidos, porque viene sin comas
                                {
                                    NombreTitular = dr["NombreTitular"].ToString();
                                }

                                DNITitular = dr["DNITitular"].ToString();
                                var EmailTitular = dr["ValorNuevo"].ToString();
                                var Calle = "";
                                var emailViejo = dr["valorOriginal"].ToString();

                                for (int j = 0; j < 8; j++)
                                {
                                    chars[j] = allowedChars[rd.Next(0, allowedChars.Length)];
                                }
                                Password = string.Join("", chars);

                                NombreTitular = NombreTitular.Replace("'", "");
                                Calle = Calle.Replace("'", "");

                                if (EmailTitular != "" && DNITitular != "")
                                {
                                    if (WS.ExisteUsuarioZDocs(EmailTitular) == "" && WS.ExisteEmpresaZDocs(DNITitular) == "")
                                    {
                                        try
                                        {
                                            IdEmpresa = WS.CrearEmpresaZDocs(NombreTitular, DNITitular, Calle, "", "Illes Balears", "ES", "", EmailTitular, "", "");
                                            IdUsuario = WS.CrearUsuarioZDocs(NombreTitular, Apellidos, EmailTitular, "", Password, DNITitular, "1024", "", DateTime.UtcNow.Date.ToString(), Password);
                                            WS.CrearUsuarioEmpresaZDocs(IdUsuario, IdEmpresa, false, true); //administrador, principal

                                            EnviarCorreo(EmailTitular, "Alta en la Plataforma ZDocs", CrearCuerpoCorreoAlta("Aguas de Marratxi", Password), "");
                                            if (IdEmpresa == "-1" || IdUsuario == "-1")
                                            {
                                                arrayIncidencias += DNITitular + "|";
                                                numIncidencias++;
                                            }
                                            else
                                            {
                                                Num_Empresas += 1;
                                            }
                                        }
                                        catch (Exception ex)
                                        {
                                            arrayIncidencias += DNITitular + "|";
                                            numIncidencias++;
                                        }
                                    }
                                    else //ya existe el usuario o empresa
                                    {
                                        //actualizamos el usuario
                                        string query = "";
                                        if (!string.IsNullOrEmpty(emailViejo))
                                        {
                                            query = "SELECT TOP 1 idUsuario FROM Usuarios WHERE email='" + emailViejo + "'";
                                        }
                                        else
                                        {//si no tiene mail buscamos por DNI
                                            query = "SELECT TOP 1 idUsuario FROM Usuarios WHERE dni='" + DNITitular + "'";
                                        }

                                        int idUsuario =  Int32.Parse(DatabaseConnection.executeScalarString(query, CommandType.Text, CadenaConexionAnywhereSinProvider));
                                        if (idUsuario > 0)
                                        {
                                            query = "UPDATE USUARIOS SET Nombre='" + NombreTitular + "', Apellidos='" + Apellidos + "', email='" + EmailTitular + "' WHERE idUsuario=" + idUsuario;
                                            int result = DatabaseConnection.executeNonQueryInt(query, CommandType.Text, CadenaConexionAnywhereSinProvider);
                                        }
                                        else
                                        {
                                            arrayIncidencias += emailViejo + " - ";
                                        }



                                    }
                                }
                                i++;
                            }
                            catch (Exception ex)
                            {
                                arrayIncidencias += DNITitular + "|";
                                numIncidencias++;

                            }
                        }
                        Incidencias = "Numero de lineas Procesadas= " + Num_Empresas + ", Incidencias: " + numIncidencias;
                        return Incidencias;
                    }
                }

            }
            catch (Exception ex)
            {
                return ex.Message;
            }
        }









        internal static string ExisteEmpresaZDocs(string dni)
        {
            return WS.ExisteEmpresaZDocs(dni);
        }


        internal static string ExisteUsuarioZDocs(string email)
        {
            return WS.ExisteUsuarioZDocs(email);
        }




        internal static string crearUsuarioEmpresa(string idUsuario, string idEmpresa)
        {
            return WS.CrearUsuarioEmpresaZDocs(idUsuario, idEmpresa, false, true); //administrador, principal
        }

        public static string SubidaPorCarpetas(string Ruta)
        {

            string arrayIncidencias = "";

            string[] directorioInicial = Ruta.Split('\\');

            //Para coger la ruta sin el nombre del Fichero
            string directorio2 = Ruta.Substring(0, Ruta.IndexOf(directorioInicial[directorioInicial.Count() - 1]));

            string FicheroErrores = directorio2 + "Errores" + DateTime.Now.ToString().Replace("/", "").Replace(":", "") + ".txt";
            string FicheroNoEncontrados = directorio2 + "NoEncontrados" + DateTime.Now.ToString().Replace("/", "").Replace(":", "") + ".txt";

            if (!System.IO.Directory.Exists(directorio2 + "\\PROCESADOS"))
                System.IO.Directory.CreateDirectory(directorio2 + "\\PROCESADOS");

            if (!System.IO.Directory.Exists(directorio2 + "\\ERRORES"))
                System.IO.Directory.CreateDirectory(directorio2 + "\\ERRORES");
            string FicheroOrigen = Ruta;
            string fileName = "";
            Ruta = directorio2;


            foreach (var directorio in Directory.GetDirectories(Ruta))
            {

                string idUsuario = "";
                string idEmpresa = "";

                string nombreDirectorio = directorio.Split('\\')[directorio.Split('\\').Length - 1];

                idUsuario = getIdUsuario(nombreDirectorio);
                idEmpresa = getIdEmpresa(idUsuario);
                if (idUsuario == "-1" || idEmpresa == "-1")
                {

                }
                else
                {
                    foreach (var fichero in Directory.GetFiles(directorio))
                    {
                        if (fichero.Contains(".pdf"))
                        {
                            byte[] file = File.ReadAllBytes(fichero);
                            string filename = fichero.Split('\\')[fichero.Split('\\').Length - 1];
                            string resultado = subirFichero(file, filename, idUsuario, "", idEmpresa, ".PDF", FicheroErrores);
                            //si no ha habido un error
                            if (!string.IsNullOrEmpty(resultado))
                            {
                                System.IO.Directory.CreateDirectory(directorio2 + "PROCESADOS" + "\\" + nombreDirectorio);
                                File.Move(fichero, directorio2 + "PROCESADOS" + "\\" + nombreDirectorio + "\\" + filename);

                            }
                        }
                    }
                    if ((Directory.GetDirectories(directorio).Length + Directory.GetFiles(directorio).Length) == 0)
                    {
                        Directory.Delete(directorio);
                    }
                }


            }

            return arrayIncidencias;
        }


        public static string AltaFicheros(string Ruta)
        {


            string arrayIncidencias = "";

            try
            {
                string[] directorio = Ruta.Split('\\');
                string IdDocumento = "";

                //Para coger la ruta sin el nombre del Fichero
                string directorio2 = Ruta.Substring(0, Ruta.IndexOf(directorio[directorio.Count() - 1]));

                string FicheroErrores = directorio2 + "Errores" + DateTime.Now.ToString().Replace("/", "").Replace(":", "") + ".txt";
                string FicheroNoEncontrados = directorio2 + "NoEncontrados" + DateTime.Now.ToString().Replace("/", "").Replace(":", "") + ".txt";

                if (!System.IO.Directory.Exists(directorio2 + "\\PROCESADOS"))
                    System.IO.Directory.CreateDirectory(directorio2 + "\\PROCESADOS");

                if (!System.IO.Directory.Exists(directorio2 + "\\ERRORES"))
                    System.IO.Directory.CreateDirectory(directorio2 + "\\ERRORES");
                string FicheroOrigen = Ruta;
                //string[] Fichero = File.ReadAllLines(FicheroOrigen);
                DirectoryInfo Directorio = new DirectoryInfo(Ruta);
                string fileName = "";
                foreach (var Fich in Directorio.GetFiles())
                {
                    try
                    {

                        directorio2 = Ruta+"\\";
                        Byte[] PDF = null;
                        fileName = DateTime.Now.Year + "/" + DateTime.Now.Month.ToString().PadLeft(2, '0') + ".pdf";
                        PDF = File.ReadAllBytes(directorio2 + Fich);
                        string idUsuario = getIdUsuario(Fich.ToString().Replace(".pdf", ""));
                        if (Int32.Parse(idUsuario) < 0)
                        {
                            arrayIncidencias += fileName + "|";
                        }
                        else
                        {
                            string idEmpresa = getIdEmpresa(idUsuario).Split('|')[1];

                            IdDocumento = subirFichero(PDF, fileName, idUsuario, "", idEmpresa, "pdf", FicheroErrores);
                            if (IdDocumento.Equals("error") || IdDocumento.Equals("") || IdDocumento.Equals("-1")) //error al subir el fichero
                            {
                                arrayIncidencias += "";
                                System.IO.Directory.Move(directorio2 + Fich, directorio2 + "ERRORES\\" + Fich);
                            }
                            else
                            {
                                System.IO.Directory.Move(directorio2 + Fich.ToString(), directorio2 + "PROCESADOS\\" + Fich.ToString());
                                EnviarCorreo(Fich.ToString().Replace(".pdf", ""), "Factura trimestral subida.", CrearCuerpoCorreoAlta("Aguas de Marratxi Subida", ""), "");
                                //EnviarCorreo("lpueyo@zertifika.com", "Factura trimestral subida.", CrearCuerpoCorreoAlta("Aguas de Marratxi Subida", ""), "");
                                //enviar mail con con PDF adjunto
                            }
                        }
                    }
                    catch (Exception ex)
                    {
                        arrayIncidencias = arrayIncidencias;
                    }
                }
            }
            catch (Exception e)
            {
                return e.Message;
            }


            return arrayIncidencias;

        }

        public static string altaUsuario(string Nombre, string Apellidos, string Email, string PuestoTrabajo, string Password, string Dni, string IdDistribuidor, string TokenTemporal,
            string FechaAlta, string PasswordEncriptada)
        {
            string idUsuario = WS.CrearUsuarioZDocs(Nombre, Apellidos, Email, PuestoTrabajo, Password, Dni, IdDistribuidor, TokenTemporal, FechaAlta, PasswordEncriptada);
            if (!idUsuario.Equals("-1"))
            {
                WF.encriptaClavesUser("Zertifika", Int32.Parse(idUsuario));
            }

            return idUsuario;
        }

        public static string altaEmpresa(string Nombre, string Cif, string Calle, string Poblacion, string Provincia, string Pais, string Cp, string Email, string Telefono, string Web)
        {

            string idEmpresa = WS.CrearEmpresaZDocs(Nombre, Cif, Calle, Poblacion, Provincia, Pais, Cp, Email, Telefono, Web);

            return idEmpresa;
        }

        /*
        public static List<string> getUsuersByEmail(string Email)
        {
            string query = "SELECT baneado, idUsuario, Nombre, Apellidos, email, fechaalta FROM Usuarios WHERE email LIKE '%" + Email + "%'";
            DataTable datos = DatabaseConnection.executeNonQueryDT(query, CommandType.Text, CadenaConexionAnywhereSinProvider);
            List<string> resultado = new List<string>();
            for (int i = 0; i < datos.Rows.Count; i++)
            {
                string Baneado = "NO BANEADO";
                if (datos.Rows[i].ItemArray[0].ToString().Equals("True"))
                {
                    Baneado = "BANEADO";
                }
                resultado.Add(Baneado + " - " + datos.Rows[i].ItemArray[1].ToString() + " - " + datos.Rows[i].ItemArray[2].ToString() + " " + datos.Rows[i].ItemArray[3].ToString()
                    + " - " + datos.Rows[i].ItemArray[4].ToString() + " - " + datos.Rows[i].ItemArray[5].ToString());
            }
            return resultado;
        }
        */

        /*
        public static List<string> getUsuersByName(string Nombre)
        {

            string query = "SELECT baneado, idUsuario, Nombre, Apellidos, email, fechaalta FROM Usuarios WHERE Nombre LIKE '%" + Nombre + "%'";
            DataTable datos = DatabaseConnection.executeNonQueryDT(query, CommandType.Text, CadenaConexionAnywhereSinProvider);

            List<string> resultado = new List<string>();
            for (int i = 0; i < datos.Rows.Count; i++)
            {
                string Baneado = "NO BANEADO";
                if (datos.Rows[i].ItemArray[0].ToString().Equals("True"))
                {
                    Baneado = "BANEADO";
                }
                resultado.Add(Baneado + " - " + datos.Rows[i].ItemArray[1].ToString() + " - " + datos.Rows[i].ItemArray[2].ToString() + " " + datos.Rows[i].ItemArray[3].ToString()
                    + " - " + datos.Rows[i].ItemArray[4].ToString() + " - " + datos.Rows[i].ItemArray[5].ToString());
            }
            return resultado;
        }

        internal static List<string> getDocumentsByEmail(string email)
        {
            string query = "SELECT idDocumento, docOriginal, Documentos.NombreOriginal, Documentos.activo from Documentos inner join Usuarios on Documentos.idUsuario = Usuarios.idUsuario where email = '" + email + "'";
            DataTable datos = DatabaseConnection.executeNonQueryDT(query, CommandType.Text, CadenaConexionAnywhereSinProvider);
            List<string> resultado = new List<string>();
            for (int i = 0; i < datos.Rows.Count; i++)
            {
                string Activo = "NO ACTIVO";
                if (datos.Rows[i].ItemArray[3].ToString().Equals("True"))
                {
                    Activo = "ACTIVO";
                }
                resultado.Add(Activo + " - " + datos.Rows[i].ItemArray[0].ToString() + " - " + datos.Rows[i].ItemArray[1].ToString() + " " + datos.Rows[i].ItemArray[2].ToString());
            }
            return resultado;
        }
        */

        //--------------------------- Metodos Auxiliares----------------------------------


        public static string subirFichero(Byte[] Fichero, string NomFichero, string idusuario, string idExpediente, string idempresa, string formato, string FicheroErrores)
        {
            string resultado = "";
            try
            {
                //string Directorio = "/Inetpub/wwwroot/ZDocs-Pro/Volumenes/";

                if (Fichero != null)
                {
                    String Fec = DateTime.Today.Year.ToString() + DateTime.Today.Month.ToString() + DateTime.Today.Day.ToString();
                    string Fecha = string.Format("{0:yyyyMMdd}", DateTime.Today);

                    string ObligadaLectura = "False";
                    byte[] kk = null;
                    String[] aux = NomFichero.Split('.');
                    // String formato = aux[aux.Length - 1];
                    string err = "";

                    string idDoc = WS.insertarDocEnBBDD(NomFichero, idusuario, formato, idempresa, "", "");

                    //if (!string.IsNullOrEmpty(TagActual) & TagActual != "undefined")
                    //    Funciones.AsignaTipo(idDoc, TagActual, "Documentos");
                    string Ruta = "00000000";
                    Ruta = Ruta.Substring(0, 8 - idDoc.Length) + idDoc;
                    //string pathString = Directorio + Ruta.Substring(0, 2) + "/" + Ruta.Substring(2, 2) + "/" + Ruta.Substring(4, 2) + "/";// +Ruta.Substring(6, 2) + "/";
                    string pathString = Ruta.Substring(0, 2) + "/" + Ruta.Substring(2, 2) + "/" + Ruta.Substring(4, 2) + "/";// +Ruta.Substring(6, 2) + "/";
                    System.IO.Directory.CreateDirectory(pathString);

                    //File.WriteAllBytes(pathString + Ruta.Substring(6, 2) + "." + formato, Fichero);
                    string resul = WS.SubirFichero(Fichero, pathString, Ruta.Substring(6, 2) + "." + formato);
                    if (resul != "1")
                        File.AppendAllText(FicheroErrores, "Error subiendo Fichero: " + Fichero);

                    if (!WS.ExisteDocumentoUsuario(idDoc, idusuario))
                    {
                        err = WS.InsertarDocumentoenDocumentosUsuario(idDoc, idusuario, "Documentos Propios", idusuario, ObligadaLectura, "", "", "", "", idempresa);
                        if (!string.IsNullOrEmpty(err))
                            File.AppendAllText(FicheroErrores, "Error insertando el Fichero: " + Fichero + " en Documentos Usuario");
                        err = "";
                    }
                    if (!string.IsNullOrEmpty(idExpediente))
                    {
                        if (!WS.ExisteDocumentoExpediente(idDoc, idExpediente))
                        {
                            err = WS.InsertarDocumentoenExpediente(idDoc, idExpediente);
                            if (!string.IsNullOrEmpty(err))
                                File.AppendAllText(FicheroErrores, "Error insertando el Fichero: " + Fichero + " en Expedientes");
                            err = "";
                        }
                        err = WS.CompartirDocumentoaUsuariosExpediente(idDoc, idExpediente, idusuario, ObligadaLectura, idempresa);
                        if (!string.IsNullOrEmpty(err))
                            File.AppendAllText(FicheroErrores, "Error insertando el Fichero: " + Fichero + " en Documentos Usuario Expedientes");
                        err = "";
                    }
                    //if (File.Exists(pathString + Ruta.Substring(6, 2) + "." + formato)) // + "." + formato  pathString + "/" + Fecha.Substring(6, 2) + "." + formato))                    
                    //    return idDoc;
                    //else
                    //{
                    //    return "-1";
                    //}
                    if (string.IsNullOrEmpty(err))
                        return idDoc;
                    else
                    {
                        File.AppendAllText(FicheroErrores, "Error subiendo Fichero");
                        return err;
                    }

                }
                return resultado;
            }
            catch (Exception e)
            {
                return "error";
                //string Fichero = "";
                //Trazas t = new Trazas("SubirDocumento", "ZDocs", Convert.ToInt16(idusuario), e.ToString(), e.InnerException + "");
                //return "-1";
            }
        }

        public static string getIdUsuario(string email)
        {
            try
            {
                return WS.getUsuersByEmail(email)[0].Split('-')[1].Trim();

            }
            catch (Exception)
            {

                return "-1";
            }
        }

        public static string getIdEmpresa(string idUsuario)
        {
            /*
            string SQL = "select Empresa.idEmpresa from Empresa inner join UsuarioEmpresa on Empresa.idEmpresa=UsuarioEmpresa.idEmpresa where idUsuario=" + idUsuario;
            DataTable datos = DatabaseConnection.executeNonQueryDT(SQL, CommandType.Text, CadenaConexionAnywhereSinProvider);
            return datos.Rows[0].ItemArray[0].ToString();
            */
            return WS.DevolverEmpresaPrincipal(idUsuario, "");
        }

        public static string EnviarCorreo(string Destino, string Asunto, string Texto, string Ruta)//,string Ruta2)
        {

            string Host = "mail.zertifika.com";
            string Origen = "info@zertifika.com";
            string Pass = "r3dM#z41#z41#z41";
            string User = "info@zertifika.com";
            if (Destino != "prevencioriscslaborals@palma.es" && Destino != "mcalvino@globalia.com" && Destino != "serprev11@globalia.com" && Destino != "previs.previs@gmail.com")
            {
                if (Destino.Contains("Zertifika.com"))
                    Destino = Destino;
                MailMessage vCorreo = new MailMessage(Origen, Destino, Asunto, Texto);
                vCorreo.IsBodyHtml = true;
                //if (System.IO.File.Exists(Ruta))// && System.IO.File.Exists(Ruta2))
                //{
                //    Attachment vAdjunto = null;
                //    if (Ruta != null)
                //    {
                //        vAdjunto = new Attachment(Ruta);
                //        vCorreo.Attachments.Add(vAdjunto);
                //    }                

                SmtpClient vCliente = new SmtpClient(Host);
                vCliente.Credentials = new System.Net.NetworkCredential(User, Pass);

                try
                {
                    vCliente.Send(vCorreo);
                    //txtenviados.Text += "\r\n" + Destino + DateTime.Now.ToString();
                    return "";

                }
                catch
                {
                    return "\r\n" + Destino + DateTime.Now.ToString();

                }
            }
            else
                return "";

        }

        public static string CrearCuerpoCorreoAlta(string Company, string Password)
        {
            string _htmlheader = "";
            string _htmlSignature = "";
            string _htmlfooter = "";

            if (Company.ToUpper() == "AGUAS DE MARRATXI")
            {

                _htmlheader = "<table width='98%' style='width: 98%;'>" +
                                           "<tr>" +
                                             "<td width='100%' style='width: 100%; padding: 30pt;'>" +
                                                 "<table style='font-family: arial; font-size: 12px; width: 631px;'>" +
                                                     "<tr><td style='background: none repeat scroll 0% 0% #1f497d; padding-left:25px; height:32px; font-size:23px; font-family: Helvetica;'><p>" +
                                                        "<span style='color: white; letter-spacing: -0.35pt;'>Aguas de Marratxi<b><span style='color: white; font-size:9px;'></span></b>" +
                                                        // " <img src='"+ImagenEnBase64+"' height='70' width='140' style='margin-left:290px;margin-top:5px'></p>" +
                                                        " <img src='https://www.signes30.com/ZDOCS/LOGO-ATM.jpg' height='70' width='140' style='margin-left:290px;margin-top:5px'></p>" +
                                                        "</tr>" +
                                                     "<tr>" +
                                                         "<td valign='top' style='border-width: medium 1pt 1pt; border-style: none solid solid; border-color: #385D8A; background: white; padding: 25px;' colspan='2'>";

                _htmlSignature = "<div style='font-size:13px; padding: 10px 0 0 25px;'>";
                _htmlSignature += "<b><label>" + "ZDocs, su nuevo servicio web de gestión de documentos:";

                _htmlSignature += "</label><a style='color:#005f98; text-decoration: none;' href='https://www.signes30.com/ZDocspro/'> pulse aqui - Signes30</b></a>";

                _htmlSignature += "</div>";

                _htmlSignature += "<div style='border-width: medium medium 1pt; border-style: none none solid; border-color: rgb(204, 204, 204); padding: 0cm;'><p><span style='font-size: 9pt;'>&nbsp;</span></p></div><p><span style='font-size: 9pt;'></span></p></div>";

                _htmlfooter = _mailFooter() + "</td>" + "</tr>" + "</table></td></tr></table>";

                return _htmlheader + "Aguas de Marratxí le ha dado de alta en Signes30, plataforma online donde, a partir de ahora recibirá sus facturas.  </br></br> Su nombre de usuario es su email, y su contraseña: " + Password + " , pero puede cambiarla cuando quiera entrando en perfil usuario " + _htmlSignature + _htmlfooter;

            }
            else if (Company.ToUpper() == "AGUAS DE MARRATXI SUBIDA")
            {

                _htmlheader = "<table width='98%' style='width: 98%;'>" +
                                           "<tr>" +
                                             "<td width='100%' style='width: 100%; padding: 30pt;'>" +
                                                 "<table style='font-family: arial; font-size: 12px; width: 631px;'>" +
                                                     "<tr><td style='background: none repeat scroll 0% 0% #1f497d; padding-left:25px; height:32px; font-size:23px; font-family: Helvetica;'><p>" +
                                                        "<span style='color: white; letter-spacing: -0.35pt;'>Aguas de Marratxi<b><span style='color: white; font-size:9px;'></span></b>" +
                                                        // " <img src='"+ImagenEnBase64+"' height='70' width='140' style='margin-left:290px;margin-top:5px'></p>" +
                                                        " <img src='https://www.signes30.com/ZDOCS/LOGO-ATM.jpg' height='70' width='140' style='margin-left:460px;margin-top:5px'></p>" + "</tr>" + "<tr>" +
                                                         "<td valign='top' style='border-width: medium 1pt 1pt; border-style: none solid solid; border-color: #385D8A; background: white; padding: 25px;' colspan='2'>";

                _htmlSignature = "<div style='font-size:13px; padding: 10px 0 0 25px;'>";

                _htmlSignature += "<b><label>" + "Estimado abonado, le comunicamos que ya tiene disponible la factura del consumo de agua correspondiente a este trimestre, accediendo al servicio web de gestión de documentos";

                _htmlSignature += "</label><a style='color:#005f98; text-decoration: none;' href='https://www.signes30.com/ZDocspro/'> Aguas de Marratxi</b></a>";

                _htmlSignature += "</div>";

                _htmlSignature += "<div style='border-width: medium medium 1pt; border-style: none none solid; border-color: rgb(204, 204, 204); padding: 0cm;'><p><span style='font-size: 9pt;'>&nbsp;</span></p></div><p><span style='font-size: 9pt;'></span></p></div>";

                _htmlfooter = _mailFooter() + "</td>" + "</tr>" + "</table></td></tr></table>";

                return _htmlheader + _htmlSignature + _htmlfooter;
                //Aqui!
            }
            else
            {

                _htmlheader = "<table width='98%' style='width: 98%;'>" +
                                           "<tr>" +
                                             "<td width='100%' style='width: 100%; padding: 30pt;'>" +
                                                 "<table style='font-family: arial; font-size: 12px; width: 631px;'>" +
                                                     "<tr><td style='background: none repeat scroll 0% 0% #1f497d; padding-left:25px; height:32px; font-size:23px; font-family: Helvetica;'><p>" +
                                                        "<span style='color: white; letter-spacing: -0.35pt;'>Aguas de Marratxi<b><span style='color: white; font-size:9px;'></span></b>" +
                                                        // " <img src='"+ImagenEnBase64+"' height='70' width='140' style='margin-left:290px;margin-top:5px'></p>" +
                                                        " <img src='https://www.signes30.com/ZDOCS/LOGO-ATM.jpg' height='70' width='140' style='margin-left:290px;margin-top:5px'></p>" + "</tr>" + "<tr>" +
                                                         "<td valign='top' style='border-width: medium 1pt 1pt; border-style: none solid solid; border-color: #385D8A; background: white; padding: 25px;' colspan='2'>";

                _htmlSignature = "<div style='font-size:13px; padding: 10px 0 0 25px;'>";
                _htmlSignature += "<b><label>" + "ZDocs, su nuevo servicio web de gestión de documentos:";

                _htmlSignature += "</label><a style='color:#005f98; text-decoration: none;' href='https://www.signes30.com/ZDocspro/'> pulse aqui - Signes30</b></a>";

                _htmlSignature += "</div>";

                _htmlSignature += "<div style='border-width: medium medium 1pt; border-style: none none solid; border-color: rgb(204, 204, 204); padding: 0cm;'><p><span style='font-size: 9pt;'>&nbsp;</span></p></div><p><span style='font-size: 9pt;'></span></p></div>";

                _htmlfooter = _mailFooter() + "</td>" + "</tr>" + "</table></td></tr></table>";

                return _htmlheader + "Se le ha dado de alta en ZDocs  </br></br> Su nombre de usuario es su email, y su contraseña: " + Password + " , pero puede cambiarla cuando quiera entrando en su perfil usuario" + _htmlSignature + _htmlfooter;
            }
        }

        public static string _mailFooter()
        {
            string _footer = "<div style='font-size:10px; color: gray;'>";
            _footer += "<label><b>" + "Navegadores recomendados: Firefox, Chrome, Safari e Internet Explorer a partir de la versión 8." + "</b></label><br />";
            //_footer += "</div>";
            return _footer;
        }















    }
}

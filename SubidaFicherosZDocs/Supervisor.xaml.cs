﻿using Microsoft.WindowsAPICodePack.Dialogs;
using System;
using System.ComponentModel;
using System.Runtime.CompilerServices;
using System.Windows;

namespace SubidaFicherosZDocs
{
    /// <summary>
    /// Interaction logic for MainWindow.xaml
    /// </summary>
    public partial class Supervisor : Window, INotifyPropertyChanged
    {

        private string _ruta;

        public string Ruta
        {
            get { return _ruta; }
            set
            {
                _ruta = value;
                NotifyPropertyChanged("Ruta");
            }
        }


        public event PropertyChangedEventHandler PropertyChanged;
        private void NotifyPropertyChanged([CallerMemberName] String propertyName = "")
        {
            if (PropertyChanged != null)
            {
                PropertyChanged(this, new PropertyChangedEventArgs(propertyName));
            }
        }

        public Supervisor()
        {
            InitializeComponent();
            Loaded += MyWindow_Loaded;
            DataContext = this;
        }

        private void MyWindow_Loaded(object sender, RoutedEventArgs e)
        {

            ComboClientes.Items.Add("Aguas de Marratxi");
            ComboClientes.Items.Add("Por cliente");
            ComboClientes.SelectedIndex = 0;
        }

        private void BTNExaminar_Click(object sender, RoutedEventArgs e)
        {
            /*
            Microsoft.Win32.OpenFileDialog dlg = new Microsoft.Win32.OpenFileDialog();
            dlg.DefaultExt = "*";
            //dlg.Filter = "JPEG Files (*.jpeg)|*.jpeg|PNG Files (*.png)|*.png|JPG Files (*.jpg)|*.jpg|GIF Files (*.gif)|*.gif";
            Nullable<bool> result = dlg.ShowDialog();
            if (result == true)
            {
                // Open document 
                Ruta = dlg.FileName;

            }
            */

            var dialog = new CommonOpenFileDialog();
            dialog.IsFolderPicker = true;
            CommonFileDialogResult result = dialog.ShowDialog();
            Ruta = dialog.FileName;

        }

        private void BTNProcesar_Click(object sender, RoutedEventArgs e)
        {
            try
            {
                string message = "";
                switch (ComboClientes.Text.ToUpper())
                {
                    case ("AGUAS DE MARRATXI"):
                        message = Funciones.CargarExcelMarratxi(Ruta);
                        break;
                    case ("POR CLIENTE"):
                        message = "Esto es para dar clientes de Alta";

                        break;
                    default:
                        break;
                }
                MessageBox.Show(message);
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
        }

        private void BTNSubirDocs_Click(object sender, RoutedEventArgs e)
        {

            string message = "";
            switch (ComboClientes.Text.ToUpper())
            {
                case ("AGUAS DE MARRATXI"):
                    message = Funciones.AltaFicheros(Ruta);
                    break;
                case ("POR CLIENTE"):
                    message = Funciones.SubidaPorCarpetas(Ruta);

                    break;
                default:
                    break;
            }





        }

        private void BTNAtras_Click(object sender, RoutedEventArgs e)
        {
            Inicio ini = new Inicio();
            ini.Show();
            this.Close();
        }

        private void BTNActualizarUsuarios_Click(object sender, RoutedEventArgs e)
        {
            try
            {
                string message = "";
                switch (ComboClientes.Text.ToUpper())
                {
                    case ("AGUAS DE MARRATXI"):
                        message = Funciones.actualizarUsuarios(Ruta);
                        break;
                    default:
                        break;
                }
                MessageBox.Show(message);
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
        }
    }
}
